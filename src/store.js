import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	signos: [
  	{signo: 'Eritema', cantidad: 0},
  	{signo: 'Edema', cantidad: 0},
  	{signo: 'Excoriacion', cantidad: 0},
  	{signo: 'Liquenificacion', cantidad: 0},
	],
	preguntas: [
		{pregunta: '¿cuánta picazón, molestia, dolor o sensación punzante ha sentido en la piel?', cantidad: 0},
		{pregunta: '¿qué tan avergonzado o cohibido se ha sentido debido a su problema de la piel?', cantidad: 0},
		{pregunta: '¿cuánto ha interferido su problema de la piel con ir de compras o cuidar la casa o el jardín?', cantidad: 0},
		{pregunta: '¿cuánta influencia ha tenido su problema de la piel en la ropa que utiliza?', cantidad: 0},
		{pregunta: '¿cuánto ha afectado su problema de la piel a sus actividades sociales o recreativas ?', cantidad: 0},
		{pregunta: '¿cuánto le ha dificultado su problema de la piel el practicar deportes?', cantidad: 0},
		{pregunta: '¿en qué medida su piel ha sido un problema en el trabajo o los estudios?', cantidad: 0},
		{pregunta: '¿en qué medida su problema de la piel le ha generado dificultades con su pareja o con cualquiera de sus amigos cercanos o familiares?', cantidad: 0},
		{pregunta: '¿en qué medida su problema de la piel le ha ocasionado dificultades sexuales?', cantidad: 0},
		{pregunta: '¿en qué medida su tratamiento para la piel le ha resultado un problema, por ejemplo, desordenando su casa o quitándole tiempo?', cantidad: 0},
	]
  },

  mutations: {
  	aumentar(state, index){
  		if (state.signos[index].cantidad < 3){
  		state.signos[index].cantidad ++}},
  	reinicio(state){
  		state.signos.forEach(elemento => {
  			elemento.cantidad = 0
		  })},
	aumentarDlqi (state, index){
		if (state.preguntas[index].cantidad < 3){
		state.preguntas[index].cantidad ++}},
	reinicioDlqi (state){
		state.preguntas.forEach(elemento => {
			elemento.cantidad = 0
		})
	}
  },

  actions: {

  }
})
